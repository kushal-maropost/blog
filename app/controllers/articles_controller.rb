class ArticlesController < ApplicationController
    def index
        articles=Article.all 
        render json: {Kushal: articles}
    end
    def show
        article=Article.find_by(id: params[:id])
        if article.present?
            render json: {Kushal: article}
       else
           render json: {message: "Article not found"}, status: 404
       end
    end
end
